package com.example.unitimes;

import android.net.Uri;
import android.provider.BaseColumns;

public final class RSSContract {

    public static final String CONTENT_AUTHORITY = "com.example.unitimes";
    public static final String PATH_RSS = "rss";

    public static class RSSItem implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(Uri.parse("content://" + CONTENT_AUTHORITY), PATH_RSS);

        public static final String TABLE_NAME = "rss_items";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_LINK = "link";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_AUTHOR = "author";
        public static final String COLUMN_ENCLOSURE = "enclosure";
        public static final String COLUMN_PUB_DATE = "pub_date";
    }
}