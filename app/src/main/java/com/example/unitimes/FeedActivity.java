package com.example.unitimes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import androidx.appcompat.app.AppCompatActivity;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
public class FeedActivity extends AppCompatActivity {

    // Define your XML tags
    private static final String TAG_ITEM = "item";
    private static final String TAG_TITLE = "title";
    private static final String TAG_LINK = "link";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        try {
            URL url = new URL("https://wwwen.uni.lu/index.php/rss/feed/fstc_news_events");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(false);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(conn.getInputStream(), "UTF_8");

            boolean insideItem = false;

            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (xpp.getName().equalsIgnoreCase(TAG_ITEM)) {
                        insideItem = true;
                    } else if (xpp.getName().equalsIgnoreCase(TAG_TITLE)) {
                        if (insideItem)
                            Log.i("FeedParser", "Title: " + xpp.nextText()); //extract the headline
                    } else if (xpp.getName().equalsIgnoreCase(TAG_LINK)) {
                        if (insideItem)
                            Log.i("FeedParser", "URL: " + xpp.nextText()); //extract the link
                    }
                } else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase(TAG_ITEM)) {
                    insideItem = false;
                }
                eventType = xpp.next();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
