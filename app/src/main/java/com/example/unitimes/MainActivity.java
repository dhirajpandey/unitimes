package com.example.unitimes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import static android.content.ContentValues.TAG;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private RadioGroup facultyRadioGroup;
    private RadioButton faculty;
    private Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        facultyRadioGroup = findViewById(R.id.radio_group_faculty);
        submitButton = findViewById(R.id.btn_submit);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get the selected radio button

                int selectedId = facultyRadioGroup.getCheckedRadioButtonId();
                faculty = (RadioButton) findViewById(selectedId);
                // Get the faculty name corresponding to the selected radio button
                String facultyName = "";
                if (faculty.getText().equals("Faculty of Science, Technology and Medicine")) {
                    facultyName = "FSTM";
                }
                if (faculty.getText().equals("Faculty of Law, Economics and Finance")) {
                    facultyName = "FDEF";
                }
                if (faculty.getText().equals("Faculty of Humanities, Education and Social Sciences")) {
                    facultyName = "FHSE";
                }
                Log.d(TAG, "FacultyName " + facultyName);
                // Pass the selected faculty name to the RSSActivity
                Intent intent = new Intent(MainActivity.this, RSSActivity.class);
                intent.putExtra("isFtsmSelected", facultyName.equals("FSTM"));
                intent.putExtra("isFdseSelected", facultyName.equals("FDEF"));
                intent.putExtra("isFhseSelected", facultyName.equals("FHSE"));
                startActivity(intent);
            }
        });
    }
}