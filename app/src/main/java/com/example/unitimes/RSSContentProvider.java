package com.example.unitimes;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class RSSContentProvider extends ContentProvider {

    private RSSDBHelper dbHelper;

    private static final int RSS_ITEMS = 100;
    private static final int RSS_ITEM_ID = 101;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(RSSContract.CONTENT_AUTHORITY, RSSContract.PATH_RSS, RSS_ITEMS);
        sUriMatcher.addURI(RSSContract.CONTENT_AUTHORITY, RSSContract.PATH_RSS + "/#", RSS_ITEM_ID);
    }

    @Override
    public boolean onCreate() {
        dbHelper = new RSSDBHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        int match = sUriMatcher.match(uri);
        Cursor cursor;

        switch (match) {
            case RSS_ITEMS:
                cursor = db.query(RSSContract.RSSItem.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case RSS_ITEM_ID:
                long id = ContentUris.parseId(uri);
                cursor = db.query(RSSContract.RSSItem.TABLE_NAME, projection, RSSContract.RSSItem._ID + "=?", new String[]{String.valueOf(id)}, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}