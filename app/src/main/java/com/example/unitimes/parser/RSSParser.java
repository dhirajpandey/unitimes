package com.example.unitimes.parser;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class RSSParser {
    private static final String TAG = "RSSParser";

    public static List<RSSItem> parse(String xmlData) {
        List<RSSItem> items = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource inputSource = new InputSource(new StringReader(xmlData));
            Document document = builder.parse(inputSource);

            NodeList itemNodes = document.getElementsByTagName("item");
            for (int i = 0; i < itemNodes.getLength(); i++) {
                Element itemElement = (Element) itemNodes.item(i);
                String title = getElementValue(itemElement, "title");
                String link = getElementValue(itemElement, "link");
                String description = getElementValue(itemElement, "description");
                String pubDate = getElementValue(itemElement, "pubDate");
                String image = getElementAttribute(itemElement, "enclosure", "url");
                RSSItem item = new RSSItem(title, link, description, pubDate, image);
                items.add(item);
            }
        } catch (Exception e) {
            Log.e(TAG, "Error parsing RSS", e);
        }
        return items;
    }

    private static String getElementValue(Element parent, String name) {
        NodeList nodes = parent.getElementsByTagName(name);
        if (nodes.getLength() > 0) {
            return nodes.item(0).getTextContent();
        }
        return "";
    }
    private static String getElementAttribute(Element parent, String name, String attribute) {
        NodeList nodeList = parent.getElementsByTagName(name);
        if (nodeList.getLength() > 0) {
            Element element = (Element) nodeList.item(0);
            return element.getAttribute(attribute);
        }
        return "";
    }
}
//public class RSSParser {
//
//    public static List<RSSItem> parse(String rssFeed) throws XmlPullParserException, IOException {
//        List<RSSItem> rssItems = new ArrayList<>();
//
//        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
//        factory.setNamespaceAware(true);
//        XmlPullParser xpp = factory.newPullParser();
//
//        xpp.setInput(new StringReader(rssFeed));
//        int eventType = xpp.getEventType();
//        RSSItem currentRssItem = null;
//        String currentTag = null;
//
//        while (eventType != XmlPullParser.END_DOCUMENT) {
//            if (eventType == XmlPullParser.START_TAG) {
//                currentTag = xpp.getName();
//                if (currentTag.equals("item")) {
//                    currentRssItem = new RSSItem();
//                }
//            } else if (eventType == XmlPullParser.TEXT) {
//                String text = xpp.getText();
//                if (currentRssItem != null) {
//                    switch (currentTag) {
//                        case "title":
//                            currentRssItem.setTitle(text);
//                            break;
//                        case "description":
//                            currentRssItem.setDescription(text);
//                            break;
//                        case "pubDate":
//                            currentRssItem.setPubDate(text);
//                            break;
//                        case "link":
//                            currentRssItem.setLink(text);
//                            break;
//                    }
//                }
//            } else if (eventType == XmlPullParser.END_TAG) {
//                currentTag = xpp.getName();
//                if (currentTag.equals("item")) {
//                    rssItems.add(currentRssItem);
//                    currentRssItem = null;
//                }
//            }
//            eventType = xpp.next();
//        }
//
//        return rssItems;
//    }
//}