package com.example.unitimes.parser;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.example.unitimes.R;
import com.example.unitimes.WebViewActivity;

import java.util.List;

public class RSSAdapter extends ArrayAdapter<RSSItem> {

    private Context context;
    private List<RSSItem> rssItems;

    public RSSAdapter(Context context, List<RSSItem> rssItems) {
        super(context, R.layout.rss_item, rssItems);
        this.context = context;
        this.rssItems = rssItems;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.rss_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageView = convertView.findViewById(R.id.image_view);
            viewHolder.titleTextView = convertView.findViewById(R.id.title_textview);
            viewHolder.descriptionTextView = convertView.findViewById(R.id.description_textview);
            viewHolder.pubDateTextView = convertView.findViewById(R.id.pub_date_textview);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        RSSItem rssItem = rssItems.get(position);
        Glide.with(context)
                .load(rssItem.getImageUrl())
                .placeholder(R.drawable.placeholder_image)
                .error(R.drawable.placeholder_image)
                .into(viewHolder.imageView);
        viewHolder.titleTextView.setText(rssItem.getTitle());
        viewHolder.titleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebViewActivity.class);
                intent.putExtra("url", rssItem.getLink());
                context.startActivity(intent);
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            viewHolder.descriptionTextView.setText(Html.fromHtml(rssItem.getDescription(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            viewHolder.descriptionTextView.setText(Html.fromHtml(rssItem.getDescription()));
        }
//        viewHolder.descriptionTextView.setText(rssItem.getDescription());
        viewHolder.pubDateTextView.setText(rssItem.getPubDate());

        return convertView;
    }

    private static class ViewHolder {
        ImageView imageView;
        TextView titleTextView;
        TextView descriptionTextView;
        TextView pubDateTextView;
    }
}