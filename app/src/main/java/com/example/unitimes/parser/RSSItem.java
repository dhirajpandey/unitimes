package com.example.unitimes.parser;

public class RSSItem {
    private String title;
    private String link;
    private String description;
    private String pubDate;
    private String image;

    public RSSItem(String title, String link, String description, String pubDate, String image) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.pubDate = pubDate;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getDescription() {
        return description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getImageUrl() {
        return image;
    }
}