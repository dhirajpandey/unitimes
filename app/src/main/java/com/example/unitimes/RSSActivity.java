package com.example.unitimes;

import static android.content.ContentValues.TAG;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.unitimes.parser.RSSAdapter;
import com.example.unitimes.parser.RSSItem;
import com.example.unitimes.parser.RSSParser;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RSSActivity extends AppCompatActivity {

    private ListView rssListView;
    private TextView header;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        rssListView = findViewById(R.id.listview_rss);
        header = findViewById(R.id.header);


        Intent intent = getIntent();
//        Bundle bundle = intent.getExtras();
//        if (bundle != null) {
//            for (String key : bundle.keySet()) {
//                Log.d(TAG, key + " : " + (bundle.get(key) != null ? bundle.get(key) : "NULL"));
//            }
//        }
        boolean isFtsmSelected = ((Intent) intent).getBooleanExtra("isFtsmSelected", false);
        boolean isFdseSelected = intent.getBooleanExtra("isFdseSelected", false);
        boolean isFhseSelected = intent.getBooleanExtra("isFhseSelected", false);

        // Build the RSS feed URL based on the selected checkboxes
        String rssUrl = "https://wwwen.uni.lu/index.php/rss/feed/";

        if (isFtsmSelected) {
            rssUrl += "fstc_news_events";
        }
        if (isFdseSelected) {
            rssUrl += "fdef_news_events";
        }
        if (isFhseSelected) {
            rssUrl += "flshase_news_events";
        }

        StringBuilder sb = new StringBuilder();
        if (isFtsmSelected) {
            sb.append("Latest Events of Faculty of Science, Technology and Medicine");
        }
        if (isFdseSelected) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append("Latest Events of Faculty of Law, Economics and Finance");
        }
        if (isFhseSelected) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append("Latest Events of Humanities, Education and Social Sciences");
        }
        header.setText(sb.toString());

        // Fetch the RSS feed
        FetchFeedTask fetchFeedTask = new FetchFeedTask();
        fetchFeedTask.execute(rssUrl);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Respond to the action bar's Up/Home button
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class FetchFeedTask extends AsyncTask<String, Void, List<RSSItem>> {

        @Override
        protected List<RSSItem> doInBackground(String... urls) {
            String rssUrl = urls[0];
            List<RSSItem> rssItems = null;
            try {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(rssUrl)
                        .addHeader("Host", "wwwen.uni.lu")
                        .build();
                Log.d(TAG, rssUrl);
                try (Response response = client.newCall(request).execute()) {
                    if (!response.isSuccessful()) {
                        throw new IOException("Unexpected code " + response);
                    }
                    String responseData = response.body().string();
                    rssItems = RSSParser.parse(responseData);
                    for (RSSItem item : rssItems) {
                        Log.d(TAG, "Title: " + item.getTitle());
                        Log.d(TAG, "Link: " + item.getLink());
                        Log.d(TAG, "Description: " + item.getDescription());
                        Log.d(TAG, "PubDate: " + item.getPubDate());
                        Log.d(TAG, "====================================");

                        ContentValues values = new ContentValues();
                        values.put("title", item.getTitle());
                        values.put("link", item.getLink());
                        values.put("description", item.getDescription());
                        values.put("pubDate", item.getPubDate());
                        getContentResolver().insert(Uri.parse("content://com.example.unitimes.rsscontentprovider/rss"), values);
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Error fetching RSS feed"+ e.getMessage());
                    rssItems = null;
                }
            } catch (Exception e) {
                Log.e(TAG, "Error parsing RSS feed", e);
                rssItems = null;
            }
            return rssItems;
        }

        @Override
        protected void onPostExecute(List<RSSItem> rssItems) {
            super.onPostExecute(rssItems);
            // Hide the progress dialog
            progressDialog.dismiss();
            if (rssItems != null && !rssItems.isEmpty()) {
                // Create a new adapter and set it to the ListView
                RSSAdapter rssAdapter = new RSSAdapter(RSSActivity.this, rssItems);
                rssListView.setAdapter(rssAdapter);
            } else {
                // Show an error message if the RSS feed is empty or could not be fetched
                Toast.makeText(RSSActivity.this, "Failed to fetch RSS feed", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Show a progress dialog while fetching the RSS feed
            progressDialog = new ProgressDialog(RSSActivity.this);
            progressDialog.setMessage("Getting all the Events...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }
}