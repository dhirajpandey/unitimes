package com.example.unitimes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RSSDBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "rss.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME = "RSSFeed";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_LINK = "link";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_AUTHOR = "author";
    public static final String COLUMN_ENCLOSURE = "enclosure";
    public static final String COLUMN_PUB_DATE = "pubDate";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    "_id INTEGER PRIMARY KEY," +
                    COLUMN_TITLE + " TEXT," +
                    COLUMN_LINK + " TEXT," +
                    COLUMN_DESCRIPTION + " TEXT," +
                    COLUMN_AUTHOR + " TEXT," +
                    COLUMN_ENCLOSURE + " TEXT," +
                    COLUMN_PUB_DATE + " TEXT)";

    public RSSDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Implement database upgrade logic here
    }
}